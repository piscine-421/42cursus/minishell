/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kyung-ki <kyung-ki@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 1970/01/01 00:00:00 by kyung-ki          #+#    #+#             */
/*   Updated: 2024/01/16 22:27:22 by kyung-ki         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/minishell.h"

static void	argmode(char *line, char *arg, char **envp, t_node *node)
{
	node->escape_skip = !ft_strchr(arg, '\'') && !ft_strchr(arg, '\"')
		&& !ft_strchr(arg, '\\');
	if (quote_check(arg, ft_strlen(arg), node) == 1 || quote_check(arg,
			ft_strlen(arg), node) == 2)
	{
		ft_putstr_fd("minishell: -c: line 1: unexpected EOF wh", STDERR_FILENO);
		if (quote_check(arg, ft_strlen(arg), node) == 1)
			ft_putendl_fd("ile looking for matching `\"'", STDERR_FILENO);
		else
			ft_putendl_fd("ile looking for matching `''", STDERR_FILENO);
		exit(2);
	}
	if (quote_check(arg, ft_strlen(arg), node) == 3)
		line = ft_strjoin(arg, "\\");
	else
		line = ft_strdup(arg);
	init_node(node);
	node->argmode = true;
	if (ft_strncmp(line, "\0", 1))
		envp = subshell(hash_handler(line, node), envp, node);
	else
		free(line);
	strarrfree(envp);
	exit(g_exit_status);
}

char	*get_line(char *str)
{
	char	*line;
	char	*line2;

	if (!MSTEST_MODE || isatty(STDIN_FILENO))
		line = readline(str);
	else
	{
		line2 = get_next_line(STDIN_FILENO);
		if (!line2)
			return (NULL);
		line = ft_strtrim(line2, "\n");
		free(line2);
	}
	return (line);
}

static char	**main_loop(char **envp, t_node *n)
{
	char	*line;
	char	*prompt;

	prompt = ft_getenv("PS1", envp);
	if (!prompt)
		prompt = "\\s-\\v\\$ ";
	prompt = expand_prompt(prompt, envp, n);
	line = get_line(prompt);
	if (prompt)
		free(prompt);
	n->line_nbr++;
	if (!line)
		exit(g_exit_status);
	init_node(n);
	n->argmode = false;
	n->escape_skip = !ft_strchr(line, '\'') && !ft_strchr(line, '\"')
		&& !ft_strchr(line, '\\');
	if (ft_strncmp(line, "\0", 1))
		envp = subshell(hash_handler(escape_handler(line, n), n), envp, n);
	return (envp);
}

int	main(int argc, char **argv)
{
	extern char	**environ;
	char		**envp;
	t_node		node;

	g_exit_status = 0;
	envp = shlvl_plus_plus(setpwd(&node, strarrdup(environ)));
	envp = ft_setenv("_", argv[0], envp);
	node.path_fallback = NULL;
	node.line_nbr = 0;
	node.aliases = malloc(sizeof(char **));
	node.aliases[0] = NULL;
	if (!ft_getenv("PATH", envp))
		node.path_fallback = ft_strdup("/usr/bin:/bin");
	set_signal();
	if (argc > 2 && !ft_strncmp(argv[1], "-c", 3))
		argmode(NULL, argv[2], envp, &node);
	envp = run_commands(envp, &node);
	while (1)
		envp = main_loop(envp, &node);
}
