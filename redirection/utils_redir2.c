/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils_redir2.c                                      :+:      :+:    :+:  */
/*                                                    +:+ +:+         +:+     */
/*   By: kyung-ki <kyung-ki@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 1970/01/01 00:00:00 by kyung-ki          #+#    #+#             */
/*   Updated: 2024/01/23 19:49:07 by kyung-ki         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/minishell.h"

int	print_err(char **args, int i, t_node *node)
{
	ft_putstr_fd("minishell: ", STDERR_FILENO);
	perror(args[i + 1]);
	g_exit_status = 1;
	node->parent_die = 1;
	if (pipe_check(args, node))
	{
		if (isp(node->ori_args[i + 1]))
			node->redir_stop = 1;
		args_left_move(args, 1);
		args_left_move(node->ori_args, 1);
		if (isp(node->ori_args[i + 1]))
			node->redir_stop = 1;
		args_left_move(args, 0);
		args_left_move(node->ori_args, 0);
		if (isp(node->ori_args[i + 1]))
			node->redir_stop = 1;
		node->child_die = 1;
		return (0);
	}
	return (1);
}

void	args_left_move(char **args, int i)
{
	while (args[i] && args[i + 1])
	{
		free(args[i]);
		args[i] = ft_strdup(args[i + 1]);
		i++;
	}
	if (!args[i + 1])
	{
		free(args[i]);
		args[i] = NULL;
	}
}

bool	is_redir(char **args, int i, int j)
{
	return (args && args[i] && (args[i][j] == '<' || args[i][j] == '>'));
}
