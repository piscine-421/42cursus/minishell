/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils_redir.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kyung-ki <kyung-ki@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 1970/01/01 00:00:00 by kyung-ki          #+#    #+#             */
/*   Updated: 2024/01/23 19:49:07 by kyung-ki         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/minishell.h"

int	redir_chk(char **args)
{
	int	i;

	i = -1;
	while (args[++i])
		if (isdlr(args[i]) || isdrr(args[i]) || islr(args[i]) || islrr(args[i])
			|| isrr(args[i]) || istlr(args[i]))
			return (1);
	return (0);
}

static void	argu_long(char **args, char **args2, int k, int len)
{
	const int	init_k = k;

	free(args[k]);
	free(args[k + 1]);
	args[k + 1] = NULL;
	while (k + 2 < len)
	{
		args[k] = ft_strdup(args[k + 2]);
		free(args[k + 2]);
		args[k++ + 2] = NULL;
	}
	if (args2)
		argu_long(args2, NULL, init_k, len);
}

static void	argu_two(char **args, char **args2, int k)
{
	free(args[k]);
	free(args[k + 1]);
	args[k] = NULL;
	args[k + 1] = NULL;
	if (args2)
		argu_two(args2, NULL, k);
}

void	argu_left_change(char **args, t_node *node)
{
	int	i;
	int	k;
	int	len;

	i = -1;
	len = strarrlen(args);
	while (args[++i])
	{
		if (isdlr(node->ori_args[i]))
		{
			k = i;
			if (k + 1 == len)
			{
				free(args[k]);
				free(node->ori_args[k]);
				args[k] = NULL;
				node->ori_args[k] = NULL;
			}
			else if (k + 2 == len)
				argu_two(args, node->ori_args, k);
			else
				argu_long(args, node->ori_args, k, len);
		}
	}
}

int	redir_excute(char **args, char **envp, t_node *node, int flag)
{
	int	pid;

	pid = -1;
	if (node->redir_flag)
	{
		if (pid <= 0)
		{
			exec_redir_child(args, envp, node, &flag);
			if (!pid)
				exit(g_exit_status);
		}
		else
			exec_redir_parents(args + node->redir_idx, envp, node, &flag);
	}
	return (flag);
}
