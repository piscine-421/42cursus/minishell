/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cmd_redir.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kyung-ki <kyung-ki@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 1970/01/01 00:00:00 by kyung-ki          #+#    #+#             */
/*   Updated: 2024/01/23 19:48:00 by kyung-ki         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/minishell.h"

int	left_redir(char **args, char **envp, int *i, t_node *node)
{
	int	fd;

	if (islr(node->ori_args[*i]))
	{
		if (access(args[*i + 1], R_OK))
			return (print_err(args, *i, node));
		fd = open(args[*i + 1], O_RDONLY, 0744);
	}
	else
		fd = open(args[*i + 1], O_CREAT | O_RDWR, 0744);
	if (fd <= 0)
		return (1);
	if (!ft_strncmp(args[0], "echo", 5) && !ft_strncmp(args[1], "./", 2)
		&& !args[2])
		node->echo_skip = 1;
	dup2(fd, STDIN_FILENO);
	if (!node->cmd && args[*i + 2] && !is_redir_check(node->ori_args[*i + 2])
		&& !exec_check(args + 2, envp, node))
		return (print_err2(args, *i));
	args_left_move(args, *i);
	args_left_move(node->ori_args, *i);
	args_left_move(args, *i);
	args_left_move(node->ori_args, *i);
	*i -= 1;
	return (close(fd), 0);
}

static bool	left_double_redir_loop(char **args, char **envp, int *i,
		t_node *node)
{
	int		bak[2];
	char	*l[3];

	l[2] = ft_getenv("PS2", envp);
	if (!l[2])
		l[2] = "> ";
	l[2] = expand_prompt(l[2], envp, node);
	bak[0] = dup(node->backup_stdin);
	bak[1] = dup(node->backup_stdout);
	dup2(bak[0], STDIN_FILENO);
	dup2(bak[1], STDOUT_FILENO);
	l[0] = get_line(l[2]);
	node->line_nbr++;
	close(bak[0]);
	close(bak[1]);
	if (!l[0])
		return (print_err3(args, node, i));
	if (!ft_strncmp(l[0], args[*i + 1], ft_strlen(args[*i + 1]) + 1))
		return (1);
	if (ft_strchr(l[0], '$') && !ft_strchr(node->ori_args[*i + 1], '\'')
		&& !ft_strchr(node->ori_args[*i + 1], '\"'))
		l[1] = expand_envvar(ft_strdup(l[0]), envp, node);
	else
		l[1] = ft_strdup(l[0]);
	return (ft_putendl_fd(l[1], node->redir_fd), free(l[0]), free(l[1]), 0);
}

int	left_double_redir(char **args, char **envp, int *i, t_node *node)
{
	node->redir_fd = open(".temp", O_CREAT | O_RDWR | O_TRUNC, 0644);
	if (isdlr(node->ori_args[*i]))
	{
		while (1)
			if (left_double_redir_loop(args, envp, i, node))
				break ;
	}
	else
		ft_putendl_fd(args[*i + 1], node->redir_fd);
	lseek(node->redir_fd, 0, SEEK_SET);
	dup2(node->redir_fd, STDIN_FILENO);
	close(node->redir_fd);
	if (!node->cmd && args[*i + 2] && !is_redir_check(node->ori_args[*i + 2])
		&& !exec_check(args + 2, envp, node))
		return (print_err2(args, *i));
	double_lmove_idx_change(args, i);
	*i += 1;
	double_lmove_idx_change(node->ori_args, i);
	return (unlink(".temp") == -1);
}

int	right_redir(char **args, int *i, t_node *node)
{
	int	fd;

	fd = open(args[*i + 1], O_WRONLY | O_CREAT | O_TRUNC, 0644);
	if (fd <= 0)
	{
		if (!node->pipe_idx)
			return (print_err(args, *i, node));
		exit(EXIT_FAILURE);
	}
	node->right_flag = 1;
	if (args[*i][0] == '2')
		dup2(fd, STDERR_FILENO);
	else
		dup2(fd, STDOUT_FILENO);
	close(fd);
	args_left_move(args, *i);
	args_left_move(node->ori_args, *i);
	args_left_move(args, *i);
	args_left_move(node->ori_args, *i);
	if (isp(node->ori_args[0]))
	{
		args_left_move(args, *i);
		args_left_move(node->ori_args, *i);
	}
	return (*i -= 1, 0);
}

int	right_double_redir(char **args, int *i, t_node *node)
{
	int	fd;

	fd = open(args[*i + 1], O_WRONLY | O_CREAT | O_APPEND, 0644);
	if (fd <= 0)
	{
		if (!node->pipe_idx)
			return (print_err(args, *i, node));
		exit(EXIT_FAILURE);
	}
	node->right_flag = 1;
	if (!node->pipe_flag)
	{
		dup2(fd, STDOUT_FILENO);
		close(fd);
	}
	args_left_move(args, *i);
	args_left_move(node->ori_args, *i);
	args_left_move(args, *i);
	args_left_move(node->ori_args, *i);
	if (isp(node->ori_args[0]))
	{
		args_left_move(args, *i);
		args_left_move(node->ori_args, *i);
	}
	return (*i -= 1, 0);
}
