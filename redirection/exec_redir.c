/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exec_redir.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kyung-ki <kyung-ki@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 1970/01/01 00:00:00 by kyung-ki          #+#    #+#             */
/*   Updated: 2024/01/23 19:48:33 by kyung-ki         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/minishell.h"

void	args_left_move_i(char **args, t_node *node)
{
	int	i;

	i = 0;
	while (++i < node->redir_idx)
	{
		args_left_move(args, 1);
		args_left_move(node->ori_args, 1);
	}
}

void	double_lmove_idx_change(char **args, int *i)
{
	args_left_move(args, *i);
	args_left_move(args, *i);
	*i -= 1;
}

int	exec_redir(char **args, char **envp, t_node *node)
{
	int	i;
	int	ret;

	ret = 0;
	if (exec_check(args, envp, node))
		node->cmd = ft_strdup(args[0]);
	i = -1;
	while (args[++i] && !isp(node->ori_args[i]) && !node->redir_stop && !ret)
		if (isdlr(node->ori_args[i]))
			ret = left_double_redir(args, envp, &i, node);
	i = -1;
	while (args[++i] && !isp(node->ori_args[i]) && !node->redir_stop && !ret)
	{
		if (islr(node->ori_args[i]) || islrr(node->ori_args[i]))
			ret = left_redir(args, envp, &i, node);
		else if (isrr(node->ori_args[i]))
			ret = right_redir(args, &i, node);
		else if (isdrr(node->ori_args[i]))
			ret = right_double_redir(args, &i, node);
		else if (istlr(node->ori_args[i]))
			ret = left_double_redir(args, envp, &i, node);
	}
	if (node->cmd)
		free(node->cmd);
	return (node->cmd = NULL, ret);
}

int	print_err2(char **args, int i)
{
	ft_putstr_fd("minishell: ", STDERR_FILENO);
	errno = ENOENT;
	perror(args[i + 2]);
	return (1);
}

int	print_err3(char **args, t_node *node, int *i)
{
	ft_putstr_fd("minishell: warning: h", STDERR_FILENO);
	ft_putstr_fd("ere-document at line ", STDERR_FILENO);
	ft_putnbr_fd(node->line_nbr, STDERR_FILENO);
	ft_putstr_fd(" delimited by end-of-file (wanted `", STDERR_FILENO);
	ft_putstr_fd(args[*i + 1], STDERR_FILENO);
	ft_putendl_fd("')", STDERR_FILENO);
	return (1);
}
