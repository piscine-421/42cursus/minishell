/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   run_commands.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 1970/01/01 00:00:00 by kyung-ki          #+#    #+#             */
/*   Updated: 2024/01/16 22:27:22 by kyung-ki         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/minishell.h"

static char	**get_file(int fd)
{
	int		i;
	char	**file;
	t_list	*lst;
	t_list	*old;

	lst = ft_lstnew(get_next_line(fd));
	while (ft_lstlast(lst)->content)
		ft_lstadd_back(&lst, ft_lstnew(get_next_line(fd)));
	file = malloc((ft_lstsize(lst) + 1) * 8);
	i = 0;
	while (lst->content)
	{
		file[i] = malloc(ft_strlen(lst->content));
		ft_strlcpy(file[i], lst->content, ft_strlen(lst->content));
		free(lst->content);
		old = lst;
		lst = lst->next;
		free(old);
		if (!file[i++])
			break ;
	}
	free(lst->content);
	free(lst);
	file[i] = 0;
	return (file);
}

static char	**run(char **file, char **envp, t_node *node)
{
	int	i;

	i = -1;
	while (file[++i])
	{
		node->escape_skip = !ft_strchr(file[i], '\'') && !ft_strchr(file[i],
				'\"') && !ft_strchr(file[i], '\\');
		envp = subshell(hash_handler(file[i], node), envp, node);
	}
	return (envp);
}

char	**run_commands(char **envp, t_node *node)
{
	char	**file;
	int		fd;
	char	*path;

	if (!ft_getenv("HOME", envp))
		return (envp);
	path = ft_strjoin(ft_getenv("HOME", envp), "/.minishellrc");
	fd = open(path, O_RDONLY | O_SYMLINK);
	free(path);
	if (fd == -1)
		return (envp);
	file = get_file(fd);
	close(fd);
	envp = run(file, envp, node);
	free(file);
	return (envp);
}
