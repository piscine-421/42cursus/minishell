/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   subshell.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 1970/01/01 00:00:00 by lcouturi          #+#    #+#             */
/*   Updated: 2024/01/21 19:03:14 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/minishell.h"

static int	new_length(char *str, char **envp, t_node *node)
{
	int	count;
	int	i;
	int	length;

	count = 0;
	i = -1;
	length = 0;
	while (str[++i])
	{
		if ((str[i] != '(' && str[i] != ')') || quote_check(str, i, node))
			length++;
		else if (str[i] == '(')
			count++;
		else if (--count < 0)
		{
			free(str);
			error_message(")", envp, node);
			return (-1);
		}
	}
	if (!count)
		return (length);
	free(str);
	error_message("newline", envp, node);
	return (-1);
}

static char	*rm_parenthesis(char *str, char **envp, t_node *node)
{
	char	*new_str;
	int		i;
	int		length;

	length = new_length(str, envp, node);
	if (length == -1)
		return (NULL);
	new_str = malloc((length + 1) * sizeof(char));
	i = -1;
	length = 0;
	while (str[++i])
		if ((str[i] != '(' && str[i] != ')') || quote_check(str, i, node))
			new_str[length++] = str[i];
	new_str[length] = '\0';
	free(str);
	return (new_str);
}

char	**subshell(char *str, char **envp, t_node *node)
{
	str = rm_parenthesis(str, envp, node);
	if (!str)
		return (envp);
	return (semicolon_handler(str, envp, node));
}
