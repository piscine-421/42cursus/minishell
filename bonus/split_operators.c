/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   split_operators.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 1970/01/01 00:00:00 by lcouturi          #+#    #+#             */
/*   Updated: 2024/01/17 18:12:44 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/minishell.h"

char	**split_operators(char *s, char **envp, t_node *n)
{
	char	*new_str;
	int		i;

	i = 0;
	while (s[i] && (((s[i] != '&' || s[i + 1] != '&') && (s[i] != '|' || s[i
						+ 1] != '|')) || quote_check(s, i, n)))
		i++;
	new_str = ft_substr(s, 0, i);
	envp = parser(new_str, envp, n);
	if (!s[i])
		return (envp);
	while (!g_exit_status && s[i] && s[i] == '|')
		while (s[i] && (s[i] != '&' || s[i + 1] != '&' || quote_check(s, i, n)))
			i++;
	while (g_exit_status && s[i] && s[i] == '&')
		while (s[i] && (s[i] != '|' || s[i + 1] != '|' || quote_check(s, i, n)))
			i++;
	if (!s[i])
		return (envp);
	new_str = ft_substr(s, i + 2, ft_strlen(s) - i + 2);
	free(s);
	envp = split_operators(new_str, envp, n);
	return (envp);
}
