/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kyung-ki <kyung-ki@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 1970/01/01 00:00:00 by kyung-ki          #+#    #+#             */
/*   Updated: 2024/01/16 22:27:22 by kyung-ki         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/minishell.h"

char	**setpwd(t_node *node, char **envp)
{
	char	*curdir[2];
	int		n;

	curdir[0] = getcwd(0, 0);
	node->pwd = ft_strdup(ft_getenv("PWD", envp));
	if (chdir(node->pwd) == -1)
		return (free(node->pwd), node->pwd = curdir[0], envp = ft_setenv("PWD",
				node->pwd, envp), envp);
	curdir[1] = getcwd(0, 0);
	if (ft_strlen(curdir[0]) < ft_strlen(curdir[1]))
		n = ft_strlen(curdir[0]) + 1;
	else
		n = ft_strlen(curdir[1]) + 1;
	if (ft_strncmp(curdir[0], curdir[1], n))
	{
		free(node->pwd);
		node->pwd = curdir[0];
		envp = ft_setenv("PWD", node->pwd, envp);
		chdir(node->pwd);
	}
	else
		free(curdir[0]);
	free(curdir[1]);
	return (envp);
}

char	**shlvl_mod(int mod, char **envp)
{
	int		newval;
	char	*tmp;

	newval = ft_atoi(ft_getenv("SHLVL", envp)) + mod;
	if (newval > 1000)
		newval = 1;
	if (newval < 0)
		newval = 0;
	tmp = ft_itoa(newval);
	envp = ft_setenv("SHLVL", tmp, envp);
	free(tmp);
	return (envp);
}

char	**shlvl_plus_plus(char **envp)
{
	char	*str;

	str = ft_getenv("SHLVL", envp);
	if (!str || ft_atol(str) != ft_atoll(str))
		str = ft_itoa(1);
	else if (ft_atoi(str) + 1 <= 0)
		str = ft_itoa(0);
	else if (ft_atoi(str) + 1 >= 1000)
	{
		ft_putstr_fd("minishell: warning: shell level (", STDERR_FILENO);
		ft_putnbr_fd(ft_atoi(str) + 1, STDERR_FILENO);
		ft_putendl_fd(") too high, resetting to 1", STDERR_FILENO);
		str = ft_itoa(1);
	}
	else
		str = ft_itoa(ft_atoi(str) + 1);
	if (!str)
	{
		strarrfree(envp);
		exit(EXIT_FAILURE);
	}
	envp = ft_setenv("SHLVL", str, envp);
	free(str);
	return (envp);
}
