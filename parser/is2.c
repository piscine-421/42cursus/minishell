/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   is.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 1970/01/01 00:00:00 by lcouturi          #+#    #+#             */
/*   Updated: 2024/01/17 18:12:44 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/minishell.h"

bool	isrr(char *str)
{
	return (str && (!ft_strncmp(str, ">", 2) || !ft_strncmp(str, "2>", 3)
			|| !ft_strncmp(str, ">|", 3) || !ft_strncmp(str, "2>|", 4)));
}

bool	istlr(char *str)
{
	return (str && !ft_strncmp(str, "<<<", 4));
}
