/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   prompt.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 1970/01/01 00:00:00 by lcouturi          #+#    #+#             */
/*   Updated: 2024/01/17 18:12:44 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/minishell.h"

static char	*expand_loop(char *fmt, char *new_fmt, char *user, char *pwd)
{
	int	j;

	j = 0;
	while (*(++fmt))
	{
		if (*fmt == '\\')
		{
			if (*(++fmt) == '$')
				new_fmt[j++] = '#' + (ft_strncmp(user, "root", 5) || !user);
			else if (*fmt == 'W')
				j += ft_strlcpy(new_fmt + j, pwd, ft_strlen(pwd) + 1);
			else if (*fmt == 's')
				j += ft_strlcpy(new_fmt + j, "minishell", 11);
			else if (*fmt == 'u')
				j += ft_strlcpy(new_fmt + j, user, ft_strlen(user) + 1);
			else if (*fmt == 'v')
				j += ft_strlcpy(new_fmt + j, "1.0", 4);
			else if (*fmt != 'h')
				fmt--;
		}
		else
			new_fmt[j++] = *fmt;
	}
	return (new_fmt);
}

static int	promptlen(char *fmt, char **envp, char *pwd, int i)
{
	int	l;

	l = 0;
	while (fmt[++i])
	{
		if (fmt[i] == '\\')
		{
			if (fmt[++i] == '$')
				l++;
			else if (fmt[i] == 'W')
				l += ft_strlen(pwd);
			else if (fmt[i] == 's')
				l += 9;
			else if (fmt[i] == 'u')
				l += ft_strlen(ft_getenv("USER", envp));
			else if (fmt[i] == 'v')
				l += 3;
			else if (fmt[i] != 'h')
				i--;
		}
		else
			l++;
	}
	return (l);
}

char	*expand_prompt(char *fmt, char **envp, t_node *node)
{
	int		l;
	char	*new_fmt;
	char	*pwd;

	if (ft_getenv("PWD", envp))
		pwd = ft_getenv("PWD", envp);
	else
		pwd = node->pwd;
	if (!ft_strncmp(pwd, "/", 2))
		pwd = "/";
	else if (!ft_strncmp(ft_getenv("HOME", envp), pwd, ft_strlen(pwd) + 1))
		pwd = "~";
	else if (ft_strchr(pwd, '/'))
		pwd = ft_strrchr(pwd, '/') + 1;
	l = promptlen(fmt, envp, pwd, -1);
	if (!l)
		return (NULL);
	new_fmt = expand_loop(fmt - 1, malloc(l + 1), ft_getenv("USER", envp), pwd);
	new_fmt[l] = '\0';
	return (new_fmt);
}
