/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   syntax.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 1970/01/01 00:00:00 by lcouturi          #+#    #+#             */
/*   Updated: 2024/01/17 18:12:44 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/minishell.h"

static char	*escape_handler_loop(char *str, char **str2, int escape,
		t_node *node)
{
	while (1)
	{
		escape = quote_check(str, ft_strlen(str), node);
		if (!escape)
			return (str);
		str2[0] = get_line("> ");
		if (!str2[0])
			return (str);
		if (str2[0][0] && str2[0][0] != '#' && escape == 3)
			str[ft_strlen(str) - 1] = '\0';
		else if ((!str2[0][0] || str2[0][0] == '#') && escape == 3)
			return (free(str2[0]), str);
		else
		{
			str2[1] = ft_strjoin(str, "\n");
			free(str);
			str = str2[1];
		}
		str2[1] = ft_strjoin(str, str2[0]);
		free(str2[0]);
		free(str);
		str = str2[1];
	}
}

char	*escape_handler(char *str, t_node *node)
{
	char	*str2[2];
	char	*str3;

	str3 = NULL;
	str3 = escape_handler_loop(str, str2, 0, node);
	return (str3);
}

static bool	c(char **args, int i, bool(f1)(char *), bool(f2)(char *))
{
	return (f1(args[i]) && f2(args[i + 1]));
}

static bool	syntax_check_loop(int i, char **a, char **envp, t_node *node)
{
	if (c(a, i, isrr, isrr) || c(a, i, islr, islr) || c(a, i, isdrr, islr)
		|| c(a, i, islr, isp) || c(a, i, isdlr, isp) || c(a, i, isdrr, isp)
		|| c(a, i, isp, isp) || c(a, i, isdlr, islr) || c(a, i, isdlr, isrr)
		|| c(a, i, islr, isrr) || c(a, i, isrr, islr) || c(a, i, isrr, isp)
		|| c(a, i, isdrr, isdrr) || c(a, i, isdrr, isrr) || c(a, i, isdlr,
			isdlr) || c(a, i, isrr, isdrr) || c(a, i, istlr, isrr) || c(a, i,
			istlr, istlr) || c(a, i, istlr, isdlr))
		return (error_message(a[i + 1], envp, node));
	else if ((isdrr(a[i]) || isrr(a[i]) || islr(a[i]) || isdlr(a[i])
			|| islrr(a[i])) && !a[i + 1])
	{
		if (node->last)
			return (error_message("newline", envp, node));
		return (error_message(";", envp, node));
	}
	else if (isp(a[i]) && !i)
		return (error_message(a[i], envp, node));
	else if (isp(a[i]) && !a[i + 1] && node->argmode && node->last)
	{
		ft_putstr_fd("minishell: -c: line 2: syntax ", STDERR_FILENO);
		ft_putendl_fd("error: unexpected end of file", STDERR_FILENO);
		g_exit_status = 2;
		return (false);
	}
	return (true);
}

bool	syntax_check(char **a, char **envp, t_node *node)
{
	int	i;

	i = -1;
	while (a[++i])
		if (!syntax_check_loop(i, a, envp, node))
			return (false);
	return (true);
}
