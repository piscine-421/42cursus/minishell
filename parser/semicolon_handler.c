/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   semicolon_handler.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 1970/01/01 00:00:00 by lcouturi          #+#    #+#             */
/*   Updated: 2024/01/17 18:12:44 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/minishell.h"

bool	error_message(char *token, char **envp, t_node *node)
{
	ft_putstr_fd(i18n(syntaxerrornearunexpectedtoken1, get_lang(envp)),
		STDERR_FILENO);
	ft_putstr_fd(token, STDERR_FILENO);
	ft_putstr_fd(i18n(syntaxerrornearunexpectedtoken2, get_lang(envp)),
		STDERR_FILENO);
	g_exit_status = 2;
	node->syntax_flag = true;
	return (false);
}

static bool	semicolon_syntax_check(char **split, char **envp, t_node *node)
{
	bool	empty;
	int		i;
	int		j;

	i = -1;
	while (split[++i])
	{
		empty = true;
		j = -1;
		while (split[i][++j])
			if (!ft_strchr(" \t", split[i][j]))
				empty = false;
		if (empty)
			return (error_message(";", envp, node));
	}
	return (true);
}

char	**semicolon_handler(char *str, char **envp, t_node *node)
{
	int		i;
	int		last;
	char	**split;

	i = 0;
	while (str[i] && ft_strchr(" \t", str[i]))
		i++;
	if (str[i] == ';')
		return (error_message(";", envp, node), envp);
	i = -1;
	split = escape_split(str, ";", node);
	if (ft_strchr(str, ';') && !semicolon_syntax_check(split, envp, node))
		return (strarrfree(split), envp);
	if (str[0] && str[ft_strlen(str) - 1] == ';')
		last = -1;
	else
		last = strarrlen(split);
	while (!node->syntax_flag && split[++i])
	{
		node->last = (i == last - 1 && last != -1);
		envp = split_operators(split[i], envp, node);
	}
	return (free(str), free(split), envp);
}
