/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hash_handler.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 1970/01/01 00:00:00 by lcouturi          #+#    #+#             */
/*   Updated: 2024/01/21 19:03:14 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/minishell.h"

char	*hash_handler(char *str, t_node *node)
{
	unsigned int	i;
	char			*new_str;

	if (ft_strlen(str))
		add_history(str);
	node->syntax_flag = false;
	i = 0;
	while (str[i] && (str[i] != '#' || quote_check(str, i, node) || (i
				&& !ft_strchr(" \t", str[i - 1]))))
		i++;
	if (i == ft_strlen(str))
		return (str);
	new_str = ft_substr(str, 0, i);
	free(str);
	return (new_str);
}
