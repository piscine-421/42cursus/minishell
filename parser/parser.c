/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 1970/01/01 00:00:00 by lcouturi          #+#    #+#             */
/*   Updated: 2024/01/17 18:12:44 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/minishell.h"

char	**find_command(char **args, char **envp, t_node *node)
{
	int	i;

	i = 0;
	while (args[i] && args[i + 1] && !isp(node->ori_args[i + 1]))
		i++;
	envp = ft_setenv("_", args[i], envp);
	if (args[0] && !ft_strncmp(args[0], "alias", 6))
		cmd_alias(args, envp, node);
	else if (args[0] && !ft_strncmp(args[0], "cd", 3))
		envp = cmd_cd(args, envp, node);
	else if (args[0] && !ft_strncmp(args[0], "exit", 5))
		cmd_exit(args, envp, node);
	else if (args[0] && !ft_strncmp(args[0], "env", 4))
		envp = cmd_env(args, envp, node);
	else if (args[0] && !ft_strncmp(args[0], "export", 7))
		envp = cmd_export(args, envp, node);
	else if (args[0] && !ft_strncmp(args[0], "pwd", 4))
		cmd_pwd(node);
	else if (args[0] && !ft_strncmp(args[0], "echo", 5))
		cmd_echo(args, node);
	else if (args[0] && !ft_strncmp(args[0], "unset", 6))
		envp = cmd_unset(args, envp, node);
	else if (args[0])
		envp = cmd_exec(args, envp, node);
	return (envp);
}

char	**parser(char *str, char **envp, t_node *node)
{
	char	**args;

	str = expand_alias(expand_envvar(str, envp, node), node);
	args = escape_split(str, " \t", node);
	args = expand_wildcard(args, envp, node);
	free(str);
	if (!args[0] || !syntax_check(args, envp, node))
		return (strarrfree(args), envp);
	node->ori_args = strarrdup(args);
	args = rm_quotes(args, node);
	if (!args)
	{
		rl_clear_history();
		strarrfree(envp);
		exit(EXIT_FAILURE);
	}
	envp = execute(args, envp, node);
	strarrfree(args);
	strarrfree(node->ori_args);
	return (envp);
}

static int	quote_check_loop(char const *s, int j, int *next, bool *doublequote)
{
	int	escape;

	escape = *next;
	if (escape != 2 && escape != 3 && s[j] == '\"')
	{
		*doublequote = escape != 1;
		*next = escape != 1;
		if (!next)
			escape = 0;
	}
	else if (escape != 1 && escape != 3 && s[j] == '\'')
		*next = ((escape != 2) * 2);
	else if (escape != 2 && escape != 3 && s[j] == '\\' && (escape != 1 || s[j
				+ 1] == '"' || s[j + 1] == '\\' || s[j + 1] == '$'))
	{
		escape = 0;
		*next = 3;
	}
	else if (escape == 3)
		*next = *doublequote;
	return (escape);
}

int	quote_check(char const *s, int i, t_node *node)
{
	int		escape;
	int		j;
	bool	doublequote;
	int		next;

	if (node->escape_skip)
		return (0);
	doublequote = false;
	escape = 0;
	next = 0;
	j = -1;
	while (++j <= i)
		escape = quote_check_loop(s, j, &next, &doublequote);
	return (escape);
}
