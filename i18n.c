/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   i18n.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 1970/01/01 00:00:00 by lcouturi          #+#    #+#             */
/*   Updated: 2024/01/17 18:12:44 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/minishell.h"

t_language	get_lang(char **envp)
{
	char	*locale;

	locale = ft_getenv("LC_ALL", envp);
	if (!locale)
		locale = ft_getenv("LANG", envp);
	if (locale && !ft_strncmp(locale, "fr_CA.UTF-8", 12))
		return (french);
	return (english);
}

static char	*i18n4(t_message arg, t_language language)
{
	if (arg == toomanyarguments)
	{
		if (language == french)
			return (": trop d'arguments\n");
		return (": too many arguments\n");
	}
	return (NULL);
}

static char	*i18n3(t_message arg, t_language language)
{
	if (arg == oldpwdnotset)
	{
		if (language == french)
			return ("minishell: cd: « OLDPWD » non défini\n");
		return ("minishell: cd: OLDPWD not set\n");
	}
	if (arg == syntaxerrornearunexpectedtoken1)
	{
		if (language == french)
			return ("minishell: erreur de syntaxe près du symbole inattendu « "\
);
		return ("minishell: syntax error near unexpected token `");
	}
	if (arg == syntaxerrornearunexpectedtoken2)
	{
		if (language == french)
			return (" »\n");
		return ("'\n");
	}
	return (i18n4(arg, language));
}

static char	*i18n2(t_message arg, t_language language)
{
	if (arg == notavalididentifier1)
	{
		if (language == french)
			return ("minishell: export: « ");
		return ("minishell: export: `");
	}
	if (arg == notavalididentifier2)
	{
		if (language == french)
			return (" » : identifiant non valable\n");
		return ("': not a valid identifier\n");
	}
	if (arg == notfound)
	{
		if (language == french)
			return (" : non trouvé\n");
		return (": not found\n");
	}
	if (arg == numericargumentrequired)
	{
		if (language == french)
			return (" : argument numérique nécessaire\n");
		return (": numeric argument required\n");
	}
	return (i18n3(arg, language));
}

char	*i18n(t_message arg, t_language language)
{
	if (arg == commandnotfound)
	{
		if (language == french)
			return (" : commande introuvable\n");
		return (": command not found\n");
	}
	if (arg == filenameargumentrequired)
	{
		if (language == french)
			return ("minishell: .: nom de fichier nécessaire en argument\n. : u\
tilisation :. nom_fichier [arguments]\n");
		return ("minishell: .: filename argument required\n.: usage: . filename\
 [arguments]\n");
	}
	if (arg == invalidaliasname1)
	{
		if (language == french)
			return ("minishell: alias: « ");
		return ("minishell: alias: `");
	}
	if (arg == invalidaliasname2)
	{
		if (language == french)
			return (" » : nom d'alias non valable\n");
		return ("': invalid alias name\n");
	}
	return (i18n2(arg, language));
}
