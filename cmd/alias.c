/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 1970/01/01 00:00:00 by lcouturi          #+#    #+#             */
/*   Updated: 2024/01/17 18:12:44 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/minishell.h"

static void	printalias(char *str)
{
	int	i;

	i = 0;
	ft_putstr_fd("alias ", STDOUT_FILENO);
	ft_putchar_fd(str[i], STDOUT_FILENO);
	while (str[++i - 1] != '=')
		ft_putchar_fd(str[i], STDOUT_FILENO);
	ft_putchar_fd('\'', STDOUT_FILENO);
	while (str[i])
		ft_putchar_fd(str[i++], STDOUT_FILENO);
	ft_putstr_fd("\'\n", STDOUT_FILENO);
}

static void	set_alias(char *arg, char **envp, t_node *node)
{
	int		i;
	char	*name;

	i = -1;
	name = ft_substr(arg, 0, ft_strchr(arg, '=') - arg);
	while (name[++i])
	{
		if (ft_strchr("\t\n \"$&\'()/;<=>\\`|", name[i]))
		{
			ft_putstr_fd(i18n(invalidaliasname1, get_lang(envp)),
				STDERR_FILENO);
			ft_putstr_fd(name, STDERR_FILENO);
			ft_putstr_fd(i18n(invalidaliasname2, get_lang(envp)),
				STDERR_FILENO);
			break ;
		}
	}
	if (!name[i])
		node->aliases = ft_setenv(name, ft_strchr(arg, '=') + 1, node->aliases);
	free(name);
}

static void	alias_loop(char *arg, char **envp, t_node *node)
{
	int		i;
	char	*name;

	if (arg[0] != '=' && ft_strchr(arg, '='))
	{
		set_alias(arg, envp, node);
		return ;
	}
	i = -1;
	while (node->aliases[++i])
	{
		name = ft_substr(node->aliases[i], 0, ft_strchr(node->aliases[i], '=')
				- node->aliases[i]);
		if (!ft_strncmp(name, arg, ft_strlen(arg)))
		{
			printalias(node->aliases[i]);
			free(name);
			return ;
		}
		free(name);
	}
	ft_putstr_fd("minishell: alias: ", STDERR_FILENO);
	ft_putstr_fd(arg, STDERR_FILENO);
	ft_putstr_fd(i18n(notfound, get_lang(envp)), STDERR_FILENO);
}

void	cmd_alias(char **args, char **envp, t_node *node)
{
	int				i;
	unsigned int	i2;
	char			*lowest;
	char			*lowest_old;

	i = 0;
	while (args[++i])
		alias_loop(args[i], envp, node);
	if (i == 1)
	{
		i2 = -1;
		while (++i2 < strarrlen(node->aliases))
		{
			lowest = NULL;
			i = -1;
			while (node->aliases[++i])
				if ((!lowest || ft_strncmp(node->aliases[i], lowest,
							ft_strlen(node->aliases[i])) < 0) && (!i2
						|| ft_strncmp(node->aliases[i], lowest_old,
							ft_strlen(node->aliases[i])) > 0))
					lowest = node->aliases[i];
			printalias(lowest);
			lowest_old = lowest;
		}
	}
}
