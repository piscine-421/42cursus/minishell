/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exec_error.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 1970/01/01 00:00:00 by lcouturi          #+#    #+#             */
/*   Updated: 2024/01/16 22:21:53 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/minishell.h"

static int	chkdir_check(DIR *check, int err, bool end)
{
	if (check)
	{
		closedir(check);
		errno = EISDIR;
	}
	else if (err == EACCES || errno == ENOTDIR)
		errno = EACCES;
	else if (!end)
		return (1);
	return (0);
}

void	chkdir(char **args, char **envp, bool end)
{
	DIR			*check;
	int			err;
	struct stat	stats;
	int			status;

	err = errno;
	check = opendir(args[0]);
	status = 0;
	stat(args[0], &stats);
	if (chkdir_check(check, err, end))
		return ;
	if (S_ISDIR(stats.st_mode))
		errno = EISDIR;
	err = errno;
	if (access(args[0], R_OK | X_OK) != 0 || err == EISDIR)
	{
		errno = err;
		ft_putstr_fd("minishell: ", STDERR_FILENO);
		perror(args[0]);
		status = 126 + (end && errno != EISDIR && errno != EACCES);
	}
	strarrfree(envp);
	strarrfree(args);
	exit(status);
}

void	exec_error(char **args, char **envp, char **paths)
{
	ft_putstr_fd("minishell: ", STDERR_FILENO);
	if (ft_strchr(args[0], '/') || !paths || !paths[0])
	{
		errno = ENOENT;
		perror(args[0]);
	}
	else
	{
		ft_putstr_fd(args[0], STDERR_FILENO);
		ft_putstr_fd(i18n(commandnotfound, get_lang(envp)), STDERR_FILENO);
	}
	strarrfree(envp);
	if (paths)
		strarrfree(paths);
	strarrfree(args);
	exit(127);
}

void	checkdot(char **args, char **envp)
{
	if (!MSTEST_MODE && !ft_strncmp(args[0], ".", 2))
	{
		ft_putstr_fd(i18n(filenameargumentrequired, get_lang(envp)),
			STDERR_FILENO);
		strarrfree(envp);
		strarrfree(args);
		exit(2);
	}
	else if (!ft_strncmp(args[0], "..", 3) || !ft_strncmp(args[0], ".", 2))
	{
		ft_putstr_fd("minishell: ", STDERR_FILENO);
		ft_putstr_fd(args[0], STDERR_FILENO);
		ft_putstr_fd(i18n(commandnotfound, get_lang(envp)), STDERR_FILENO);
		exit(127);
	}
}
