/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cd.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 1970/01/01 00:00:00 by lcouturi          #+#    #+#             */
/*   Updated: 2024/01/16 23:35:20 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/minishell.h"

char	*ft_getenv(const char *name, char **envp)
{
	int	i;

	i = 0;
	while (envp[i] && (ft_strncmp(envp[i], name, ft_strlen(name))
			|| (envp[i][ft_strlen(name)] != '=' && envp[i][ft_strlen(name)])))
		i++;
	if (!envp[i] || !ft_strchr(envp[i], '='))
		return (NULL);
	return (envp[i] + ft_strlen(name) + 1);
}

char	**ft_setenv(const char *name, const char *value, char **envp)
{
	int		i;
	int		n;
	char	*str;

	if (!value)
		str = ft_strdup(name);
	else
	{
		n = ft_strlen(name) + ft_strlen(value) + 2;
		str = malloc(n);
		ft_strlcpy(str, name, n);
		ft_strlcat(str, "=", n);
		ft_strlcat(str, value, n);
	}
	i = 0;
	if (!str)
		exit(EXIT_FAILURE);
	while (envp[i] && (ft_strncmp(envp[i], name, ft_strlen(name))
			|| (envp[i][ft_strlen(name)] != '=' && envp[i][ft_strlen(name)])))
		i++;
	if (envp[i])
		return (free(envp[i]), envp[i] = str, envp);
	return (envp = strarradd(envp, str), free(str), envp);
}

static bool	checks2(char **args, char **envp, t_node *node, bool offset)
{
	if (!ft_strncmp(args[1 + offset], "-", 2))
	{
		if (!ft_getenv("OLDPWD", envp))
		{
			g_exit_status = EXIT_FAILURE;
			ft_putstr_fd(i18n(oldpwdnotset, get_lang(envp)), STDERR_FILENO);
			return (1);
		}
		free(node->pwd);
		node->pwd = ft_strdup(ft_getenv("OLDPWD", envp));
		chdir(node->pwd);
		ft_putendl_fd(ft_getenv("OLDPWD", envp), STDOUT_FILENO);
	}
	else if (chdir(args[1 + offset]) == -1)
	{
		g_exit_status = EXIT_FAILURE;
		ft_putstr_fd("minishell: cd: ", STDERR_FILENO);
		perror(args[1 + offset]);
		return (true);
	}
	return (false);
}

static bool	checks(char **args, char **envp, t_node *node, bool offset)
{
	if (strarrlen(args) > 2 + offset)
	{
		g_exit_status = EXIT_FAILURE;
		ft_putstr_fd("minishell: cd", STDERR_FILENO);
		ft_putstr_fd(i18n(toomanyarguments, get_lang(envp)), STDERR_FILENO);
		return (true);
	}
	else if (!args[1 + offset])
	{
		if (!ft_getenv("HOME", envp))
		{
			g_exit_status = EXIT_FAILURE;
			ft_putendl_fd("minishell: cd: HOME not set", STDERR_FILENO);
			return (true);
		}
		chdir(ft_getenv("HOME", envp));
	}
	else
		return (checks2(args, envp, node, offset));
	return (false);
}

char	**cmd_cd(char **args, char **envp, t_node *node)
{
	bool	offset;

	g_exit_status = EXIT_SUCCESS;
	if (node->pipe_flag || node->pipe_idx)
		return (envp);
	offset = 0;
	if (args[1] && !ft_strncmp(args[1], "--", 3))
		offset++;
	if (args[1 + offset] && !args[1 + offset][0] && !args[2 + offset])
	{
		envp = ft_setenv("OLDPWD", ft_getenv("PWD", envp), envp);
		return (envp);
	}
	if (checks(args, envp, node, offset))
		return (envp);
	if (!args[1 + offset])
	{
		free(node->pwd);
		node->pwd = ft_strdup(ft_getenv("HOME", envp));
	}
	else if (ft_strncmp(args[1 + offset], "-", 2))
		node->pwd = newpwd(node, args[1 + offset]);
	envp = ft_setenv("OLDPWD", ft_getenv("PWD", envp), envp);
	envp = ft_setenv("PWD", node->pwd, envp);
	return (envp);
}
