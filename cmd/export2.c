/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   export2.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 1970/01/01 00:00:00 by kyung-ki          #+#    #+#             */
/*   Updated: 2024/01/23 19:53:05 by kyung-ki         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/minishell.h"

static void	printchar(char c)
{
	if (c == '\\' || c == '\"' || c == '$')
		ft_putchar_fd('\\', STDOUT_FILENO);
	if (c == '\t')
		ft_putstr_fd("$\'\\t\'", STDOUT_FILENO);
	else
		ft_putchar_fd(c, STDOUT_FILENO);
}

void	printenv(char *str)
{
	int	i;

	i = 0;
	ft_putstr_fd("declare -x ", STDOUT_FILENO);
	ft_putchar_fd(str[i], STDOUT_FILENO);
	while (str[++i - 1] != '=')
	{
		if (!str[i - 1])
		{
			ft_putchar_fd('\n', STDOUT_FILENO);
			return ;
		}
		ft_putchar_fd(str[i], STDOUT_FILENO);
	}
	if (!ft_strchr(str, '\t'))
		ft_putchar_fd('\"', STDOUT_FILENO);
	while (str[i])
		printchar(str[i++]);
	if (!ft_strchr(str, '\t'))
		ft_putstr_fd("\"\n", STDOUT_FILENO);
	else
		ft_putstr_fd("\n", STDOUT_FILENO);
}
