/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   unset.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kyung-ki <kyung-ki@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 1970/01/01 00:00:00 by kyung-ki          #+#    #+#             */
/*   Updated: 2024/01/23 19:46:47 by kyung-ki         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/minishell.h"

static int	find_envkey(char *str, char *envp)
{
	int	i;

	i = 0;
	while (str[i] && envp[i] && str[i] == envp[i] && envp[i] != '=')
		i++;
	return (str[i] == '\0' && envp[i] == '=');
}

static char	**delete_env(char *str, char **envp, t_node *node, int *flag)
{
	int	i;
	int	last;

	last = 0;
	while (envp[last])
		last++;
	if (last < 1)
		return (0);
	i = -1;
	while (envp[++i])
	{
		if (!ft_strncmp(str, "PATH", 5) && node->path_fallback)
		{
			free(node->path_fallback);
			node->path_fallback = NULL;
		}
		if (find_envkey(str, envp[i]))
		{
			free(envp[i]);
			envp[i] = ft_strdup(envp[last - 1]);
			free(envp[last - 1]);
			return (envp[last - 1] = NULL, *flag = 1, envp);
		}
	}
	return (*flag = 1, envp);
}

char	**cmd_unset(char **args, char **envp, t_node *node)
{
	int	i;
	int	flag;

	i = 0;
	flag = 1;
	if (args[1] && args[1][0] == '-')
	{
		ft_putstr_fd("minishell: unset: ", STDERR_FILENO);
		ft_putstr_fd(args[1], STDERR_FILENO);
		ft_putstr_fd(": invalid option\nunset: usage: unset [-", STDERR_FILENO);
		ft_putstr_fd("f] [-v] [-n] [name ...]\n", STDERR_FILENO);
		g_exit_status = 2;
		return (envp);
	}
	while (args[++i])
		envp = delete_env(args[i], envp, node, &flag);
	if (flag != 1)
		g_exit_status = EXIT_FAILURE;
	else
		g_exit_status = EXIT_SUCCESS;
	return (envp);
}
