/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exit.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 1970/01/01 00:00:00 by lcouturi          #+#    #+#             */
/*   Updated: 2024/01/16 22:27:22 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/minishell.h"

long	ft_atol(const char *str)
{
	long	i[3];

	if (!str)
		return (0);
	i[0] = 0;
	i[2] = 0;
	i[1] = 1;
	while ((str[i[0]] > '\b' && str[i[0]] <= '\r') || str[i[0]] == ' ')
		i[0]++;
	if (str[i[0]] == '-')
		i[1] = -1;
	i[0] += (str[i[0]] == '+' || str[i[0]] == '-');
	while (str[i[0]] >= '0' && str[i[0]] <= '9')
		i[2] = i[2] * 10 + (str[i[0]++] - '0');
	return (i[2] *= i[1], i[2]);
}

__int128	ft_atoll(const char *str)
{
	__int128	i[3];

	if (!str)
		return (0);
	i[0] = 0;
	i[2] = 0;
	i[1] = 1;
	while ((str[i[0]] > '\b' && str[i[0]] <= '\r') || str[i[0]] == ' ')
		i[0]++;
	if (str[i[0]] == '-')
		i[1] = -1;
	i[0] += (str[i[0]] == '+' || str[i[0]] == '-');
	while (str[i[0]] >= '0' && str[i[0]] <= '9')
		i[2] = i[2] * 10 + (str[i[0]++] - '0');
	return (i[2] *= i[1], i[2]);
}

static bool	ft_isalldigit(char *str)
{
	int	i;

	i = 0;
	while (str[i] && ft_strchr(" \t\f\r", str[i]))
		i++;
	i += (str[i] == '+' || str[i] == '-');
	while (str && str[i])
	{
		if (!ft_isdigit(str[i]))
			break ;
		i++;
	}
	while (str && str[i])
	{
		if (!ft_strchr(" \t", str[i]))
			return (false);
		i++;
	}
	return (true);
}

static bool	error_check(char **args, char **envp)
{
	if (ft_atol(args[1]) != ft_atoll(args[1]) || !ft_isalldigit(args[1])
		|| !args[1][0])
	{
		g_exit_status = 2;
		ft_putstr_fd("minishell: exit: ", STDERR_FILENO);
		ft_putstr_fd(args[1], STDERR_FILENO);
		ft_putstr_fd(i18n(numericargumentrequired, get_lang(envp)),
			STDERR_FILENO);
	}
	else if (strarrlen(args) > 2)
	{
		g_exit_status = EXIT_FAILURE;
		ft_putstr_fd("minishell: exit", STDERR_FILENO);
		ft_putstr_fd(i18n(toomanyarguments, get_lang(envp)), STDERR_FILENO);
		return (1);
	}
	return (0);
}

void	cmd_exit(char **args, char **envp, t_node *node)
{
	if (!node->exit_flag)
		return ;
	if (isatty(STDIN_FILENO) && !node->argmode)
		ft_putendl_fd("exit", STDOUT_FILENO);
	g_exit_status = EXIT_SUCCESS;
	if (strarrlen(args) > 1)
	{
		if (ft_isalldigit(args[1]))
			g_exit_status = ft_atoi(args[1]);
		if (error_check(args, envp))
			return ;
	}
	free(node->pwd);
	strarrfree(args);
	strarrfree(envp);
	rl_clear_history();
	exit(g_exit_status);
}
